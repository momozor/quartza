<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class RootController extends AbstractController
{
    /**
     * Quartza main homepage.
     *
     * @Route("/", name="home")
     */
    public function index()
    {
        return $this->render('root/index.html.twig');
    }

    /**
     * Quartza's about page
     * where all information lies here.
     *
     * @Route("/about", name="about")
     */
    public function about()
    {
        return $this->render('root/about.html.twig');
    }
}
