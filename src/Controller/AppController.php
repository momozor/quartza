<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Entity\Quote;
use App\Security\AuthorizeUsers;
use App\Services\Utils;
use App\Form\QuoteFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;

class AppController extends AbstractController
{
    /**
     * app index page (show all data).
     *
     * @Route("/list", name="app_list")
     */
    public function list(Utils $u)
    {
        return $this->render('app/list.html.twig',
            ['quotes' => $u->findAll()]);
    }

    /**
     * create a new quote.
     *
     * @Route("/app/new", name="app_new")
     */
    public function new(Request $request, Utils $u, EntityManagerInterface $em)
    {
        $quote = new Quote();
        $form = $this->createForm(QuoteFormType::class, $quote);
        $form->handleRequest($request);

        if ($u->isFormValidated($form)) {
            $user = $u->getReference(User::class);
            $quote->setUser($user);
            $em->persist($quote);
            $em->flush();

            return $this->render('app/op_success.html.twig',
                ['title' => '200 - Creation Successful',
                 'message' => 'Quote Creation Successful!', ]);
        }

        return $this->render('app/new_form.html.twig', ['form' => $form->createView()]);
    }

    /**
     * show a particular quote.
     *
     * @Route("/app/show/{id}", name="app_show", requirements={"id"="\d+"})
     */
    public function show($id, AuthorizeUsers $au, Utils $u)
    {
        $quote = $u->find($id);

        if (!$quote) {
            throw $this->createNotFoundException('The quote does not exist');
        }

        $sameUser = $au->isSameAuthor($quote->getUser()->getId());

        return $this->render('app/show.html.twig',
         ['quote' => $quote,
          'sameUser' => $sameUser,
         ]);
    }

    /**
     * update an existing quote.
     *
     * @Route("/app/update/{id}", name="app_update", requirements={"id"="\d+"})
     */
    public function update($id, Request $request, AuthorizeUsers $au, Utils $u, EntityManagerInterface $em)
    {
        $quote = $u->find($id);

        if (!$quote) {
            throw $this->createNotFoundException('This quote does not exist!');
        }

        if (!$au->isSameAuthor($quote->getUser()->getId())) {
            throw $this->createNotFoundException('You are not original owner of this record');
        }

        $quote->setContent($quote->getContent());
        $quote->setQuoter($quote->getQuoter());

        $form = $this->createForm(QuoteFormType::class, $quote);

        $form->handleRequest($request);

        if ($u->isFormValidated($form)) {
            $em->persist($quote);
            $em->flush();

            return $this->render('app/op_success.html.twig',
                ['title' => '200 - Update Successful',
                 'message' => 'Quote Update Successful!', ]);
        }

        return $this->render('app/update_form.html.twig', ['form' => $form->createView()]);
    }

    /**
     * delete an existing quote.
     *
     * @Route("/app/delete/{id}", name="app_delete", requirements={"id"="\d+"})
     */
    public function delete($id, AuthorizeUsers $au, Utils $u, EntityManagerInterface $em)
    {
        $quote = $u->find($id);

        if (!$quote) {
            throw $this->createNotFoundException('No such record found');
        }

        if (!$au->isSameAuthor($quote->getUser()->getId())) {
            throw $this->createNotFoundException('You are not the original owner of this record');
        }

        $em->remove($quote);
        $em->flush();

        return $this->render('app/op_success.html.twig',
            ['title' => '200 - Deletion Successful',
                'message' => 'Quote Deletion Successful!', ]);
    }
}
