<?php

declare(strict_types=1);

namespace App\Security;

use Symfony\Component\Security\Core\Security;
use Psr\Log\LoggerInterface;

class AuthorizeUsers
{
    public function __construct(Security $security, LoggerInterface $logger)
    {
        $this->security = $security;
        $this->logger = $logger;
    }

    /**
     * authorize if authenticated user is same as the original author.
     *
     * @param id user id in database
     */
    public function isSameAuthor(int $id)
    {
        if ($this->security->getUser()->getId() == $id) {
            $this->logger->debug('original author of the record is equal to the authenticated user',
                ['id' => $id]);

            return true;
        }

        $this->logger->debug('original author is not equal to authenticated user',
            ['id' => $id]);

        return false;
    }
}
