<?php

declare(strict_types=1);

namespace App\Services;

use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;
use App\Entity\Quote;
use Symfony\Component\Security\Core\Security;

class Utils
{
    private $repository;

    public function __construct(LoggerInterface $logger, EntityManagerInterface $entityManager, Security $security)
    {
        $this->logger = $logger;
        $this->repository = $entityManager->getRepository(Quote::class);
        $this->entityManager = $entityManager;
        $this->security = $security;
    }

    /**
     * validate quote form inputs.
     */
    public function isFormValidated($form)
    {
        if ($form->isSubmitted() && $form->isValid()) {
            return true;
        }

        return false;
    }

    /**
     * get all data from quote.
     */
    public function findAll()
    {
        return $this->repository->findAll();
    }

    /**
     * get a record by quote id.
     */
    public function find($id)
    {
        return $this->repository->find($id);
    }

    /**
     * get user reference by user class.
     */
    public function getReference($user_class)
    {
        return $this->entityManager->getReference($user_class,
            $this->security->getUser()->getId());
    }
}
