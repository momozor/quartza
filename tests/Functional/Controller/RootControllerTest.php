<?php

declare(strict_types=1);
namespace App\Tests\Functional\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RootControllerTest extends WebTestCase
{
    public function testShowIndex()
    {
        $client = static::createClient();

        $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testShowAbout()
    {
        $client = static::createClient();

        $client->request('GET', '/about');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
