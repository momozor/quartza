[![Build Status](https://travis-ci.org/momozor/quartza.svg?branch=master)](https://travis-ci.org/momozor/quartza)
# quartza
Quote away!

## Prerequisites
* PHP 7.2+
* composer
* Required common PHP extensions (sqlite3, mysql, mbstring, intl, bcrypt, etc)

## Install and Run

## Development Environment (requires sqlite3 extension)
- `git clone https://github.com/momozor/quartza`
- `cd quartza`
- `composer install`
- Run SQLite3 database migration with `php bin/console doctrine:migrations:migrate`
- Run the development server with `php bin/console server:run 3000`
- Launch and navigate your web browser to http://localhost:3000

## Running tests (requires phpunit package)
* `php bin/phpunit`

## Acknowledgements

* <a href="https://pixabay.com/photos/clipboards-papers-text-quotes-924044/">Image</a> by <a href="https://pixabay.com/users/StockSnap-894430/">StockSnap</a> on Pixabay

## Authors
* Momozor

## License
This project is licensed under the AGPL-3.0 license. See LICENSE file for further details.
